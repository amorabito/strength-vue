// Not sure why, but need .default w/ import
const PouchDB = require('pouchdb').default
const pouchFind = require('pouchdb-find').default

const log = require('../util/logger').default

// Add the pouch-find plugin for mongo-like searching w/ $regex
PouchDB.plugin(pouchFind)

// Create local database
const localDbName = 'strength'
const local = new PouchDB(localDbName)

// Fetch VUE_APP_REMOTE_DB environment variable. If not provided DB will not sync
const remoteDb = process.env.VUE_APP_REMOTE_DB || false
// Setup synchronization
// TODO: auto refresh on pulls?
local.setupRemoteSync = (userId) => {
  if (remoteDb && !local.syncSetup && userId) {
    local.get('_design/user')
      .then(() => {
        local.syncSetup = PouchDB.sync(
          localDbName,
          remoteDb,
          {
            live: true, retry: true,
            filter: 'user/by_user_id',
            query_params: { 'user_id' : userId }
          })
          .on('change',  log.log.bind(log))
          .on('paused',  log.log.bind(log))
          .on('active',  log.log.bind(log))
          .on('denied',  log.log.bind(log))
          .on('complete',  log.log.bind(log))
          .on('error', log.error.bind(log));
      })
      .catch((err) => {
        local.put({
          _id: '_design/user',
          filters: {
            by_user_id: function(doc, req) {
              return doc._id.indexOf(req.query.user_id) > -1;
            }.toString()
          }
        })
        .then(() => {
          local.syncSetup = PouchDB.sync(
            localDbName,
            remoteDb,
            {
              live: true, retry: true,
              filter: 'user/by_user_id',
              query_params: { 'user_id' : userId }
            })
            .on('change',  log.log.bind(log))
            .on('paused',  log.log.bind(log))
            .on('active',  log.log.bind(log))
            .on('denied',  log.log.bind(log))
            .on('complete',  log.log.bind(log))
            .on('error', log.error.bind(log));
        })
      })
  }
}

export default {
  install(Vue) {
    Vue.prototype.$db = local;
  }
};
