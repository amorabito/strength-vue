import Vue from 'vue'

import './plugins/vuetify'
import AuthPlugin from './plugins/auth';
import DatabasePlugin from './plugins/database'

import App from './App.vue'
import router from './router'

Vue.config.productionTip = false
Vue.prototype.$isDev = process.env.VUE_APP_IS_DEV
Vue.prototype.$allowRecalculate = process.env.VUE_APP_ALLOW_RECALCULATE
Vue.prototype.$allowImportExport = process.env.VUE_APP_ALLOW_IMPORT_EXPORT

Vue.use(require('vue-moment'))
Vue.use(AuthPlugin)
Vue.use(DatabasePlugin)

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
