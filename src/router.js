import Vue from 'vue'
import VueRouter from 'vue-router'

import auth from './util/auth/authService'

import Home from './views/Home'
import Workouts from './views/Workouts'
import WorkoutSummary from './views/WorkoutSummary.vue'
import ExerciseLibrary from './views/ExerciseLibrary.vue'
import Exercise from './views/Exercise.vue'
import Callback from './components/Callback'

Vue.use(VueRouter)

const routes = [
  { path: '/callback', component: Callback },
  { path: '/workouts', component: Workouts },
  { path: '/workout/:id', component: WorkoutSummary },
  { path: '/exercises', component: ExerciseLibrary },
  { path: '/exercise/:id', component: Exercise },
  { path: '/', component: Home }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes, 
    scrollBehavior (to, from, savedPosition) {
      return new Promise((resolve) => {
        var position = savedPosition
        if (!position) {
          position = { x: 0, y: 0 }
        }

        setTimeout(() => resolve(position), 50)
      })
    }
})

router.beforeEach(async (to, from, next) => {
  if (to.path === '/' || to.path === '/callback' || auth.isAuthenticated()) {
    return next()
  }

  await auth.renewTokens()
  if (auth.isAuthenticated()) {
    return next()
  }

  auth.login({ target: to.path })
})

export default router;