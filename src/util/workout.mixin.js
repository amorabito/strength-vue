import exerciseMixin from './exercise.mixin'

const pouchCollate = require('pouchdb-collate')

export default {
    mixins: [exerciseMixin],
    data: () => ({
        //TODO: Deprecated
        statTypes: ['volume','oneRepMax','weight'],
        //TODO: Deprecated
        statFormula: {
            volume: (set) => +set.weight * +set.reps,
            oneRepMax: (set) => +set.weight * ( 1 + ( 0.033 * +set.reps ) ),
            weight: (set) => +set.weight,
        },
        //TODO: Deprecated
        prDisplayText: ['Volume Max', 'One Rep Max', 'Weight Max'],
        exerciseStatMap : {
            'Barbell'             : ['volume','oneRepMax','weight'],
            'Dumbbell'            : ['volume','oneRepMax','weight'],
            'Machine/Other'       : ['volume','oneRepMax','weight'],
            'Weighted bodyweight' : ['maxReps','weight'],
            'Assisted bodyweight' : ['maxReps','weight'],
            'Reps Only'           : ['maxReps'],
            'Cardio'              : ['maxTime', 'maxDistance'],
            'Duration'            : ['maxTime'],
        },
        statConfig : {
            volume: {
                formula: (set) => +set.weight * +set.reps,
                displayText: 'Volume Max',
            },
            oneRepMax: {
                formula: (set) => +set.weight * ( 1 + ( 0.033 * +set.reps ) ),
                displayText: 'One Rep Max',
            },
            weight: {
                formula: (set) => +set.weight,
                displayText: 'Weight Max',
            },
            maxReps: {
                formula: (set) => +set.reps,
                displayText: 'Rep Max',
            },
            maxTime: {
                formula: (set) => +set.time,
                displayText: 'Max Time',
            },
            maxDistance: {
                formula: (set) => +set.distance,
                displayText: 'Max Distance',
            },
        },
    }),
    methods: {
        getWorkout(id) {
            return this.getDoc(id)
        },
        getWorkouts() {
            return this.getAllDocs()
        },
        getMyStats() {
            const userId = this.$auth.getUserId()
            return this.findDocIdsLike(`stats|${userId}`)
        },
        getStat(exerciseName) {
            const userId = this.$auth.getUserId()
            return this.getDoc(`stats|${userId}|${exerciseName}`)
        },
        saveMyStats(stats) {
            if (!stats._id) {
                const userId = this.$auth.getUserId()
                stats._id = `stats|${userId}`
            }

            return this.saveDoc(stats)
        },
        saveMyStat(stat) {
            if (!stat._id) {
                const userId = this.$auth.getUserId()
                const exerciseName = stat.exerciseName
                stat._id = `stats|${userId}|${exerciseName}`
            }

            return this.saveDoc(stat)
        },
        getActiveWorkoutInfo() {
            const userId = this.$auth.getUserId()
            const settingsId = `active|${userId}`
            return this.getDoc(settingsId)
        },
        saveActiveWorkoutInfo(activeInfo) {
            if (!activeInfo._id) {
                const userId = this.$auth.getUserId()
                activeInfo._id = `active|${userId}`
            }

            return this.saveDoc(activeInfo)
        },
        saveWorkout(workout)  {
            if (!workout._id) {
                const userId = this.$auth.getUserId()
                const workoutDate = new Date(workout.startedAt)
                workout._id = pouchCollate.toIndexableString(
                    [userId, 'Workout', workoutDate.getTime()])
            }
            return this.saveDoc(workout);
        },
        deleteWorkout(workout)  {
            return this.deleteDoc(workout);
        },
        completeWorkout(workout) {
            let exerciseIndexesToRemove = []

            workout.endedAt = new Date().toJSON()
            workout.isActive = false
            workout.exercises.forEach((e, i) => {
                e.sets = e.sets.filter(s => s.complete)
                if (e.sets.length === 0) {
                    exerciseIndexesToRemove.push(i)
                }
            })

            if (exerciseIndexesToRemove.length > 0) {
                workout.exercises = workout.exercises.slice().filter((e, i) => exerciseIndexesToRemove.indexOf(i) === -1)
            }
    
            const statPromise = this.getMyStats()
                .then((stats) => this.updateStats(workout, stats))
                .catch(() => this.updateStats(workout, []))
    
            return statPromise.then(() => this.saveWorkout(workout))
        },
        async updateStats (workout, stats) {
            const { exercises } = workout
            const workoutId = workout._id

            // Get the set of unique exercise names
            const exerciseNames = Array.from(new Set(exercises.map(e => e.name)))
            const exerciseTypeMap = {}
            exercises.forEach(e => exerciseTypeMap[e.name] = e.type)

            workout.duration = workout.endedAt ? new Date(workout.endedAt).getTime() - new Date(workout.startedAt).getTime() : 0

            var totalVolume = 0, numPrs = 0

            // For all the exercise names, find the create/update statistic
            for (var i = 0; i < exerciseNames.length; i++) {
                // The exercise name
                const exerciseName = exerciseNames[i]

                // The exercise statistics
                var exerciseStatistic
                try {
                    exerciseStatistic = await this.getStat(exerciseName)
                } catch(e) {
                    exerciseStatistic = false
                }
                
                // The statistics
                var prs = { }
                var indexes = { }

                // Find statistic values
                exercises.forEach((e, _eIndex) => {
                    if (e.name === exerciseName) {
                        const statTypes = e.type ? this.exerciseStatMap[e.type] : this.statTypes
                        e.sets.forEach((s, sIndex) => {
                            totalVolume += s.weight ? +s.weight * +s.reps : 0

                            statTypes.forEach((type) => {
                                const val = this.statConfig[type].formula(s)
                                if (!prs[type] || val > prs[type]) {
                                    prs[type] = val
                                    indexes[`${type}EIndex`] = _eIndex
                                    indexes[`${type}SIndex`] = sIndex
                                }
                            })
                        })
                    }
                })

                // If new statistic
                if (!exerciseStatistic) {
                    // Create statistic
                    exerciseStatistic = { 
                        exerciseName,
                        workouts: [],
                        prs: {}
                    }
                }

                // Add workout to exercise statistic
                exerciseStatistic.workouts.push(workoutId)

                const eType = exerciseTypeMap[exerciseName]
                const statTypes = eType ? this.exerciseStatMap[eType] : this.statTypes
                statTypes.forEach((type) => {
                    // Get current PR
                    const pr = exerciseStatistic.prs[type]

                    var newPr = false
                    // Check if new PR
                    if (!pr) {
                        exerciseStatistic.prs[type] = {
                            history: [],
                            val: prs[type],
                            date: new Date().toJSON()
                        }

                        newPr = true
                    }
                    else if (pr.val < prs[type]) {
                        // If so, move current PR to history
                        pr.history.push({
                            val: pr.val,
                            date: pr.date
                        })

                        // Update current PR values
                        pr.val = prs[type]
                        pr.date = new Date().toJSON()

                        newPr = true
                    }

                    if (newPr) {
                        // Update workout set with PR flag
                        const exerciseIndex = indexes[`${type}EIndex`]
                        const setIndex = indexes[`${type}SIndex`]
                        const set = workout.exercises[exerciseIndex].sets[setIndex]
                        
                        if (!set.prs) set.prs = {}
                        set.prs[type] = true
                        
                        numPrs++
                    }
                })

                await this.saveMyStat(exerciseStatistic)
            }

            workout.totalVolume = totalVolume
            workout.numPrs = numPrs

            return this.getMyStats()
        },
        recalculateStatistics() {
            const statReCreatePromise = this.getMyStats()
                .then((stats) => Promise.all(stats.map(this.deleteDoc)))
                .catch((err) => {
                    if (err.status === 404) {
                      return new Promise((res) => res())
                    }
                })
            
            statReCreatePromise.then(() => this.getMyWorkouts([{_id: 'asc'}]))
                .then(async (workouts) => {
                    var exerciseNames = []
                    for (var i = 0; i < workouts.length; i++) {
                        const w = workouts[i]

                        w.exercises.forEach(e => {
                            exerciseNames.push(e.name)
                            e.sets.forEach(s => s.prs = {})
                        })

                        const cleanedW = await this.saveWorkout(w)
                        const stats = await this.getMyStats()
                        await this.updateStats(cleanedW, stats)
                        await this.saveWorkout(cleanedW)
                    }

                    const stats = await this.getMyStats()
                    const statExerciseNames = stats.map(e => e.exerciseName)

                    statExerciseNames.forEach(async (name) => {
                        if (exerciseNames.findIndex(e => e === name) < 0) {
                            await this.saveExercise({name})
                        }
                    })
                })
        },
    }
}