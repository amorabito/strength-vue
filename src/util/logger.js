/* eslint-disable no-console */
function Logger() {
    this.log = console.log
    this.debug  = console.debug
    this.error = console.error
}

export default new Logger()