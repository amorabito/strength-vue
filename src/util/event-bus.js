// To Use
// import { EventBus } from '../util/event-bus.js';
// 
// Broadcasting:  EventBus.$emit('eventName', value)
// Receiving:     EventBus.$on('eventName', (value) => { ...code })

import Vue from 'vue';
export const EventBus = new Vue();