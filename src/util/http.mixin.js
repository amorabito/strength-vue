const axios = require('axios')
const serverUrl = process.env.VUE_APP_SERVER_URL || '';

export default {
    methods: {
        get:    (endpoint)          => axios.get(`${serverUrl}/${endpoint}`),
        delete: (endpoint)          => axios.delete(`${serverUrl}/${endpoint}`),
        post:   (endpoint, body)    => axios.post(`${serverUrl}/${endpoint}`, body),
    }
}