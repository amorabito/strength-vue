import auth0 from 'auth0-js'
import { EventEmitter } from 'events'

const webAuth = new auth0.WebAuth({
  domain: process.env.VUE_APP_AUTH0_DOMAIN,
  redirectUri: `${window.location.origin}/callback`,
  clientID: process.env.VUE_APP_AUTH0_CLIENT_ID,
  responseType: 'id_token',
  scope: 'openid profile email'
})

const localStorageKey = 'loggedIn'
const loginEvent = 'loginEvent'

class AuthService extends EventEmitter {
  idToken = null
  profile = null
  tokenExpiry = null

  renewing = false

  login(customState) {
    webAuth.authorize({
      appState: customState
    })
  }

  logOut() {
    localStorage.removeItem(localStorageKey)

    this.idToken = null
    this.tokenExpiry = null
    this.profile = null

    webAuth.logout({
      returnTo: `${window.location.origin}`
    })

    this.emit(loginEvent, { loggedIn: false })
  }

  handleAuthentication() {
    return new Promise((resolve, reject) => {
      webAuth.parseHash((err, authResult) => {
        if (err) {
          this.emit(loginEvent, {
            loggedIn: false,
            error: err,
            errorMsg: err.statusText
          })
          reject(err)
        } else {
          this.localLogin(authResult)
          resolve(authResult.idToken)
        }
      })
    })
  }

  isAuthenticated() {
    return (
      Date.now() < this.tokenExpiry &&
      localStorage.getItem(localStorageKey) === 'true'
    )
  }

  isIdTokenValid() {
    return this.idToken && this.tokenExpiry && Date.now() < this.tokenExpiry
  }

  getIdToken() {
    return new Promise((resolve, reject) => {
      if (this.isIdTokenValid()) {
        resolve(this.idToken)
      } else if (this.isAuthenticated()) {
        this.renewTokens().then(authResult => {
          resolve(authResult.idToken)
        }, reject)
      } else {
        resolve()
      }
    })
  }

  getUserId() {
    return this.profile.user_id
  }

  localLogin(authResult) {
    this.idToken = authResult.idToken
    this.profile = authResult.idTokenPayload

    // Convert the expiry time from seconds to milliseconds,
    // required by the Date constructor
    this.tokenExpiry = new Date(this.profile.exp * 1000)

    localStorage.setItem(localStorageKey, 'true')

    this.emit(loginEvent, {
      loggedIn: true,
      profile: authResult.idTokenPayload,
      state: authResult.appState || {}
    })
  }

  renewPromise = null
  renewTokens() {
    if (this.renewing) {
      return this.renewPromise
    }

    this.renewing = true
    this.renewPromise = new Promise((resolve, reject) => {
      if (localStorage.getItem(localStorageKey) !== 'true') {
        this.renewing = false
        // this.renewPromise = null
        return reject('Not logged in')
      }

      webAuth.checkSession({}, (err, authResult) => {
        this.renewing = false
        // this.renewPromise = null

        if (err) {
          reject(err)
        } else {
          this.localLogin(authResult)
          resolve(authResult)
        }
      })
    })

    return this.renewPromise
  }
}

const service = new AuthService()

service.setMaxListeners(5)

export default service
