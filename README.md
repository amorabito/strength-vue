# Strength
## Features
1. Create Workouts
1. Create Exercises
1. Add Exercises to Workout
1. Add Sets to Exercise
1. Rename Workouts
1. User Profile

## Wishlist
1. Picture handwriting recognition import
1. Video recognition
1. Data Analytics
1. Exercise Types (extend Exercise Object)
1. Workout Templates

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
npm start
gcloud app deploy
```
Note: To synchronize with remote couchdb / pouchdb database create a _.env variable_ *VUE_APP_REMOTE_DB*
```
VUE_APP_REMOTE_DB=https://username:password@hostnameOrIP:6984/database
```

To integrate login capabilities with Auth0 create _.env variables_ *VUE_APP_AUTH0_DOMAIN* and *VUE_APP_AUTH0_CLIENT_ID*
```
VUE_APP_AUTH0_DOMAIN=<your Auth0 domain>
VUE_APP_AUTH0_CLIENT_ID=<your Auth0 client id>
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
