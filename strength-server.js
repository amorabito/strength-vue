// var http = require('http');

// var nStatic = require('node-static');

// var fileServer = new nStatic.Server('./dist');

// http.createServer(function (req, res) {
    
//     fileServer.serve(req, res);

// }).listen(8080);


/* eslint-disable no-console */

require('dotenv').config();

const express = require('express');

const { join } = require('path');

// const morgan = require('morgan');

const app = express();



// app.use(morgan('dev'));

app.use(express.static(join(__dirname, 'dist')));



app.use((_, res) => {

  res.sendFile(join(__dirname, 'dist', 'index.html'));

});



app.listen(process.env.PORT, () => console.log(`Listening on port ${process.env.PORT}`));

